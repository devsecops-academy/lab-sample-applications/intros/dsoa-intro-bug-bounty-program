# Bug bounty report: Insecure Direct Object References - URL Tampering

**Submitted by:** R1ddl3

**Date submitted:** 05/07/2023

**Description**

IDORs are an access control vulnerability that takes place when users can directly access objects with their input. This is caused by an exposed reference to an internal implementation object. An Insecure Direct Object Vulnerability can allow horizontal privilege escalation and are also sometimes associated with vertical privilege escalation.
There are different kinds of IDOR attacks - this was a URL Tampering attack where I modified the URL on the client side. 

**Reproduction**

When I logged in to my account, I checked whether I could access other pages directly by editing the pathname. I wanted to check whether I was able to access other data that is not supposed to be accessible from my account.

On the employee payslip page at https://cinematics.employees.com/payslips/124 my userID (124) was in the pathname.

![IDOR_PATH](report_images/IDOR_1.png/)

I edited the pathname to change the userID to 253. The page refreshed with the new user's information.

![IDOR_PATH](report_images/IDOR_2.png/)

Because there was no access control check, I was able to manipulate the reference, allowing me to gain unauthorized access to another user's data.

**Exploitability and Impact**

This vulnerability is easily exploitable, with a moderate technical impact. IDOR attacks can be used to read, change, or delete other user data.
