# Bug bounty report: Injection


**Submitted by:** K1d G3nius

**Date submitted:** 10/07/2023


In this injection attack I injected untrusted input into your application. This was processed by the interpreter as part of the command it was running.

**Reproduction**

I executed an injection attack on https://cinematics.employees.com/wages to test whether I could read the contents of the server's files. In the 'Weekly Wage' input field I ran the following command:
```
res.end(require('fs').readdirSync('.').toString())
```
This enabled me to read the current directory's contents:

![INJECTION_RES](report_images/wages_injection.png/)
**Exploitability**

This vulnerability can have a severe impact on your business as many different kinds of commands can be executed using the same methods - to disable or end processes - temporarily halting business activities, expose data, or execute other kinds of attacks.

If I am able to see the source code, I could offer some suggestions to strengthen your input fields. Otherwise, try to follow best practices like using safe parsing methods to parse JSON input and to use the strict mode for your functions.
