# Bug bounty report: Cross-Site Scripting(XSS)

**Submitted by:** h@ker-nxtdoor

**Date submitted:** 07/07/2023

**Summary**

A Cross-Site Scripting(XSS) vulnerability takes place when an application sends untrusted data to a web browser without proper validation. XSS scripts are executed in a victims' browser, which may redirect users to other sites or allows access to sensitive information held by the browser, such as cookies.

**Technical details**

To test whether there are any XSS vulnerabilities, I used the same method that I did when searching for Server Side Injection vulnerabilities, I injected an alert into the input fields. 

On the https://cinematics.employees.com/profile page, there is a form to fill in/ edit your personal details. In the 'Last Name' field I was able to inject the following command to create an alert:

```
<script>alert(document.cookie)</script>
```

![COMMAND](report_images/cinematics_xss.png/)


When this command was submitted, the code was executed and the alert popped up.


![RESULT](report_images/cinematics_xss_res.png/)

**Impact**

Depending on what kind of commands are run, XSS attacks can have different implications. For example, as an attacker I could steal a user's session cookies and impersonate them. I could also use it to spread malware, deface your website, etc.

**Recommendation**

As I mentioned in my previous bug bounty report on Server Side JS Injection, it's important to sanitize any input to ensure that you have cleaned and filtered data inputs from users, APIs and webservices by removing unnecessary characters and strings.

It's also important to have context-sensitive output encoding, as every browser has different rendering rules for their context. For other recommendations, see the OWASP's [cheat sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html).

