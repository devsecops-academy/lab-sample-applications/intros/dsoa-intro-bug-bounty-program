# Bug bounty report: Server Side JS Injection

**Submitted by:** h@ker-nxtdoor

**Date submitted:** 07/07/2023

**Summary**

When a user inputs data it travels to the web server in various ways, like a POST or GET request, or cookies. In some cases, this input can be modified and manipulated to access the server. This is known as an injection, where the code is sent to the interpreter as part of another query or command. 

For example, using certain functions to process inputs from the user allows the attacker to inject their malicious code on the server and execute it.

**Technical details**

To check for Server Side JS Injection vulnerabilities, I tested the input fields available on your website by trying to kill the running process.

To do this I ran the following command:

```
process.kill(process.pid) 
```

![COMMAND](report_images/cinematics_inj.png/)

Some input fields were sanitized. However on the 'Weekly Wages' page at https://cinematics.employees.com/wages in the input fields I was able to submit my script and execute it. This led to the website crashing.

![RESULT](report_images/cinematics_inj_res.png/)

**Impact**

This vulnerability can have a severe impact on your application and organization in general if the attacker brings your site down or access confidential data like your clients' personal information.

**Recommendation**

In general, I would recommend not to use functions like eval(), setTimeout(), and setInterval() as it makes it easy for an attacker to run arbitrary code.

Secondly, sanitize any input to ensure that you have cleaned and filtered data inputs from users, APIs and webservices by removing unnecessary characters and strings so that no malicious code is injection into the system. 
