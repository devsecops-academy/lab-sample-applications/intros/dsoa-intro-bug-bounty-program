# Bug bounty report: HTTP Flood Attack


**Submitted by:** PR4NKST3R

**Date submitted:** 08/07/2023


**Description**

An HTTP flood is a Distributed Denial(DDoS) of Service attack. HTTP clients (for example, a web browser) send a large amount of GET or POST HTTP requests. These requests look like valid internet traffic making it tricky to tell the difference. A large amount of requests are sent, forcing the server to allocate as many resources as possible to these requests - and denying other users access.

**Reproduction**

I created a HTTP GET flood by using a botnet to send a large amount of GET requests on the login page at http://cinematics.employees.com/login. This overwhelmed the server. When I tried to access the webpage from another machine as a 'legitimate user', I was not able to as the server was overwhelmed with requests already.

For the duration of the time that the DDoS attack is running, your website will not be available for legitimate users.

**Mitigation**

Implement a captcha test to check whether a bot is accessing your website. This computational challenge will help mitigate potential attacks. There are other strategies as well, like using a web application firewall.
